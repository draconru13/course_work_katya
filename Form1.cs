﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
namespace kyrsovaya_katyshkins3043
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public int vertecesCount = 0;
        public int cellsNumber = 0;
        public int minVertecesCount = 0;


        public int[,] AdjMatrix;
        public int[,] _AdjMatrix;
        public ArrayList graphComponents = new ArrayList();
        static int d = (int)DateTime.Now.Ticks;
        Random rnd = new Random(d);
        public string Line = Environment.NewLine;
        int H, W;


        private static int Nmax;

        private int[,] A;
        private int[,] A_Eiler;
        private int[] Stack;
        int[][] Ribs;
        List<Rebra> edgeArchive = new List<Rebra>();
        public int PointOfDeparture;
        public List<int> Price = new List<int>();
        private void Form1_Load(object sender, EventArgs e)
        {
            vershiny_ValueChanged(sender, e);
            W = pictureBox1.Width;
            H = pictureBox1.Height;


        }

        private void vershiny_ValueChanged(object sender, EventArgs e)
        {
            
            vertecesCount = (int)vershiny.Value;
            cellsNumber = (int)vershiny.Value + 1;

            Nmax = vertecesCount;
            int N_St = Nmax * (Nmax - 1) / 2;
            Ribs = new int[Nmax - 1][];
            A_Eiler = new int[Nmax, Nmax];
            Stack = new int[0];

            Matrix.ColumnCount = cellsNumber;
            Matrix.RowCount = cellsNumber;

            AdjMatrix = new int[cellsNumber, cellsNumber];

            Matrix.Rows[0].Cells[0].Style.BackColor = SystemColors.AppWorkspace;
            Matrix.Rows[0].Cells[0].Style.ForeColor = SystemColors.AppWorkspace;

            for (int i = 1; i < cellsNumber; i++)
            {
                Matrix[0, i].Value = i;
                Matrix[0, i].Style.BackColor = Color.LightGray;
                Matrix[i, 0].Value = Matrix[0, i].Value;
                Matrix[i, 0].Style.BackColor = Matrix[0, i].Style.BackColor;
            }

            //заполнение диагонали
            for (int i = 1; i < cellsNumber; i++)
            {
                Matrix[i, i].Value = 0;
                Matrix[i, i].Style.BackColor = Color.Thistle;
            }
        }

        private void drawField_Paint(object sender, PaintEventArgs e)
        {

        }

        private void drawingGraph_Click(object sender, EventArgs e)
        {
            Clear.Enabled = true;
            main_vershina.Enabled = true;
            main_vershina.Minimum = 1;
            main_vershina.Maximum = vertecesCount;
            Find.Enabled = true;

            Applying();
            pictureBox1.Refresh();

            Graphics gr = pictureBox1.CreateGraphics();
            Vershina[] verteces = new Vershina[vertecesCount]; //массив вершин
            Rebra[] edges = new Rebra[vertecesCount]; //массив рёбер

            int size = 20; //размер вершины
            int x = rnd.Next(size, pictureBox1.Width - size);
            int y = rnd.Next(size, pictureBox1.Height - size);

            for (int i = 0; i < vertecesCount; i++)
            {
                switch (i)
                {
                    case 0:
                        x = rnd.Next(W / 4 - size);
                        y = rnd.Next(H / 2 - size);
                        break;
                    case 1:
                        x = rnd.Next(W / 4 - size, W / 2 - size);
                        y = rnd.Next(H / 2 - size);
                        break;
                    case 2:
                        x = rnd.Next(W / 2 - size, 3 * W / 4 - size);
                        y = rnd.Next(H / 2 - size);
                        break;
                    case 3:
                        x = rnd.Next(3 * W / 4 - size, W - size);
                        y = rnd.Next(H / 2 - size);
                        break;
                    case 4:
                        x = rnd.Next(W / 4 - size);
                        y = rnd.Next(H / 2 - size, H - size);
                        break;
                    case 5:
                        x = rnd.Next(W / 4 - size, W / 2 - size);
                        y = rnd.Next(H / 2 - size, H - size);
                        break;
                    case 6:
                        x = rnd.Next(W / 2 - size, 3 * W / 4 - size);
                        y = rnd.Next(H / 2 - size, H - size);
                        break;
                    case 7:
                        x = rnd.Next(3 * W / 4 - size, W - size);
                        y = rnd.Next(H / 2 - size, H - size);
                        break;
                }
                verteces[i] = new Vershina(x, y, size, size, (i + 1).ToString());
            }
            
            //создание, отрисовка рёбер
            for (int i = 0; i < vertecesCount; i++)
            {
                for (int j = i + 1; j < vertecesCount; j++)
                {
                    if (_AdjMatrix[i, j] != 0)
                    {
                        edges[i] = new Rebra(verteces[i].X + size / 2, verteces[i].Y + size / 2, verteces[j].X + size / 2, verteces[j].Y + size / 2, _AdjMatrix[i, j].ToString());
                        edges[i].Draw(gr);
                        edgeArchive.Add(edges[i]);
                    }
                }
            }

            foreach (var v in verteces)
                v.Draw(gr);
            Init();


        }

        //инициализация вершин и массива
        private void Init()
        {
            Nmax = vertecesCount;
            A = _AdjMatrix;
        }

        // Строит каркас минимального веса
        private void FindTree(int[,] A_Eiler)
        {
            Bibl Sp = new Bibl();
            int min = 100;
            int l = 0, t = 0;
            for (int i = 0; i < Nmax - 1; i++)
                for (int j = 1; j < Nmax; j++)
                    if ((A[i, j] < min) && (A[i, j] != 0))
                    {
                        min = A[i, j];
                        l = i;
                        t = j;
                    }
            A_Eiler[l, t] = A[l, t];
            A_Eiler[t, l] = A[t, l];
            Sp.Add(l + 1);
            Sp.Add(t + 1);

            int ribIndex = 0;
            Ribs[ribIndex] = new int[2];
            Ribs[ribIndex][0] = l + 1;
            Ribs[ribIndex][1] = t + 1;
            ribIndex++;

            while (!Sp.Contains(1, Nmax))
            {
                min = 100;
                l = 0; t = 0;
                for (int i = 0; i < Nmax; i++)
                    if (Sp.Contains(i + 1))
                        for (int j = 0; j < Nmax; j++)
                            if (!Sp.Contains(j + 1) && (A[i, j] < min) && (A[i, j] != 0))
                            {
                                min = A[i, j];
                                l = i;
                                t = j;
                            }
                A_Eiler[l, t] = A[l, t];
                A_Eiler[t, l] = A[t, l];
                Sp.Add(l + 1);
                Sp.Add(t + 1);

                Ribs[ribIndex] = new int[2];
                Ribs[ribIndex][0] = l + 1;
                Ribs[ribIndex][1] = t + 1;
                ribIndex++;
            }
        }

        //Поиск пути
        private void FindWay(int v)
        {
            for (int i = 0; i < Nmax; i++)
                if (A_Eiler[v, i] != 0)
                {
                    A_Eiler[v, i] = 0;
                    FindWay(i);
                }
            int[] temp = (int[])Stack.Clone();
            Stack = new int[Stack.Length + 1];
            for (int i = 0; i < temp.Length; i++)
                Stack[i] = temp[i];
            Stack[Stack.Length - 1] = v + 1;
        }

        int s = 0;
        // Вывод результата
        private void OutPut()
        {
            Bibl Way = new Bibl();
            int i, pred_v, Cost = 0;
            textBox1.Text += "Маршрут: ";
            textBox1.Text += Stack[0] + "--->";
            Way.Add(Stack[0]);
            pred_v = Stack[0];
            for (i = 1; i < Stack.Length; i++)
                if (!Way.Contains(Stack[i]))
                {
                    textBox1.Text += Stack[i] + "--->";
                    Way.Add(Stack[i]);
                    Cost += A[pred_v - 1, Stack[i] - 1];
                    pred_v = Stack[i];
                    Price.Add(pred_v);
                }
            textBox1.Text += string.Format("{0} ", Stack[0]);

            Cost += A[pred_v - 1, Stack[0] - 1];

            textBox1.Text += Line;
            textBox1.Text += "Длина пути: " + Cost;
            textBox1.Text += Line;
            textBox1.Text += Line;
            s = Cost;
        }


        private void gridMatrix_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //зеркальное отображение значений
            Matrix[e.RowIndex, e.ColumnIndex].Value = Matrix[e.ColumnIndex, e.RowIndex].Value;

        }
     
        private void gridMatrix_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //запрет на изменение значений по диагонали
            if (e.RowIndex == e.ColumnIndex)
                e.Cancel = true;


            //запрет на редактирование ячеек
            //отвечающих за нумерацию строк и столбцов
            for (int i = 1; i < cellsNumber; i++)
            {
                if (e.RowIndex == 0 && e.ColumnIndex == i || (e.RowIndex == i && e.ColumnIndex == 0))
                    e.Cancel = true;
            }
        }

        private void gridMatrix_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                ((DataGridView)sender).SelectedCells[0].Selected = false;
            }
            catch { }


        }

        private void gridMatrix_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Matrix.BeginEdit(true);
        }

     

        //сохранение введённых значений в таблице
        //запись данных в матрицу смежности
        private void Applying()
        {
            //матрица смежности без учета нумерации строк и столбцов
            for (int i = 1; i < cellsNumber; i++)
            {
                for (int j = 1; j < cellsNumber; j++)
                {
                    AdjMatrix[i, j] = Convert.ToInt32(Matrix[i, j].Value);
                }
            }

            _AdjMatrix = new int[vertecesCount, vertecesCount];

            for (int i = 0; i < AdjMatrix.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < AdjMatrix.GetLength(1) - 1; j++)
                {
                    _AdjMatrix[i, j] = AdjMatrix[i + 1, j + 1];
                }
            }
        }

        private void cmbPointOfDeparture_SelectedIndexChanged(object sender, EventArgs e)
        {
            Find.Enabled = true;
        }

        int start;
        private void Find_Click(object sender, EventArgs e)
        {
            
            int start = (int)main_vershina.Value - 1;
            FindTree(A_Eiler);
            FindWay(start);
            OutPut();

            Price.Clear();

            Nmax = vertecesCount;
            int N_St = Nmax * (Nmax - 1) / 2;
            Ribs = new int[Nmax - 1][];
            A_Eiler = new int[Nmax, Nmax];
            Stack = new int[0];

        }

        private void main_vershina_ValueChanged(object sender, EventArgs e)
        {
            Find.Enabled = true;
            start = (int)main_vershina.Value;
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            label2.Focus();
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }



        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
             if (pictureBox1.Image != null)
            {
                SaveFileDialog savedialog = new SaveFileDialog();
                savedialog.Title = "Сохранить картинку как...";
                savedialog.OverwritePrompt = true;
                savedialog.CheckPathExists = true;
                savedialog.Filter = "Image Files(*.BMP)|*.BMP|Image Files(*.JPG)|*.JPG|Image Files(*.GIF)|*.GIF|Image Files(*.PNG)|*.PNG|All files (*.*)|*.*";
                savedialog.ShowHelp = true;
                if (savedialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        pictureBox1.Image.Save(savedialog.FileName);
                    }
                    catch
                    {
                        MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void Clear_Click(object sender, EventArgs e)
        {
            pictureBox1.Invalidate();
            textBox1.Text = string.Empty;
        }
    }
}
