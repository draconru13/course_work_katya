﻿namespace kyrsovaya_katyshkins3043
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Matrix = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Clear = new System.Windows.Forms.Button();
            this.main_vershina = new System.Windows.Forms.NumericUpDown();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Find = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.vershiny = new System.Windows.Forms.NumericUpDown();
            this.drawingGraph = new System.Windows.Forms.Button();
            this.draw = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.закрытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.Matrix)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.main_vershina)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vershiny)).BeginInit();
            this.draw.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Matrix
            // 
            this.Matrix.AllowUserToAddRows = false;
            this.Matrix.AllowUserToDeleteRows = false;
            this.Matrix.AllowUserToResizeColumns = false;
            this.Matrix.AllowUserToResizeRows = false;
            this.Matrix.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.Matrix.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Matrix.ColumnHeadersVisible = false;
            this.Matrix.Location = new System.Drawing.Point(6, 100);
            this.Matrix.MultiSelect = false;
            this.Matrix.Name = "Matrix";
            this.Matrix.RowHeadersVisible = false;
            this.Matrix.RowHeadersWidth = 51;
            this.Matrix.Size = new System.Drawing.Size(192, 131);
            this.Matrix.TabIndex = 0;
            this.Matrix.TabStop = false;
            this.Matrix.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.gridMatrix_CellBeginEdit);
            this.Matrix.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridMatrix_CellMouseClick);
            this.Matrix.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridMatrix_CellValueChanged);
            this.Matrix.SelectionChanged += new System.EventHandler(this.gridMatrix_SelectionChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Clear);
            this.groupBox1.Controls.Add(this.main_vershina);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.Find);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.vershiny);
            this.groupBox1.Controls.Add(this.drawingGraph);
            this.groupBox1.Controls.Add(this.Matrix);
            this.groupBox1.Location = new System.Drawing.Point(12, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(214, 375);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Панель управления";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // Clear
            // 
            this.Clear.BackColor = System.Drawing.Color.Plum;
            this.Clear.Enabled = false;
            this.Clear.Location = new System.Drawing.Point(105, 275);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(93, 23);
            this.Clear.TabIndex = 17;
            this.Clear.TabStop = false;
            this.Clear.Text = "Очистить";
            this.Clear.UseVisualStyleBackColor = false;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // main_vershina
            // 
            this.main_vershina.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.main_vershina.Enabled = false;
            this.main_vershina.Location = new System.Drawing.Point(5, 251);
            this.main_vershina.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.main_vershina.Name = "main_vershina";
            this.main_vershina.Size = new System.Drawing.Size(58, 20);
            this.main_vershina.TabIndex = 16;
            this.main_vershina.TabStop = false;
            this.main_vershina.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.main_vershina.ValueChanged += new System.EventHandler(this.main_vershina_ValueChanged);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.textBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.textBox1.Location = new System.Drawing.Point(5, 305);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(189, 64);
            this.textBox1.TabIndex = 14;
            this.textBox1.TabStop = false;
            this.textBox1.Enter += new System.EventHandler(this.textBox1_Enter);
            // 
            // Find
            // 
            this.Find.BackColor = System.Drawing.Color.Plum;
            this.Find.Enabled = false;
            this.Find.Location = new System.Drawing.Point(5, 275);
            this.Find.Name = "Find";
            this.Find.Size = new System.Drawing.Size(93, 23);
            this.Find.TabIndex = 0;
            this.Find.TabStop = false;
            this.Find.Text = "Найти путь";
            this.Find.UseVisualStyleBackColor = false;
            this.Find.Click += new System.EventHandler(this.Find_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Выберите вершину:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Матрица путей:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(133, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "Выберите число вершин:";
            // 
            // vershiny
            // 
            this.vershiny.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.vershiny.Location = new System.Drawing.Point(6, 32);
            this.vershiny.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.vershiny.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.vershiny.Name = "vershiny";
            this.vershiny.Size = new System.Drawing.Size(57, 20);
            this.vershiny.TabIndex = 0;
            this.vershiny.TabStop = false;
            this.vershiny.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.vershiny.ValueChanged += new System.EventHandler(this.vershiny_ValueChanged);
            // 
            // drawingGraph
            // 
            this.drawingGraph.BackColor = System.Drawing.Color.Plum;
            this.drawingGraph.Location = new System.Drawing.Point(6, 58);
            this.drawingGraph.Name = "drawingGraph";
            this.drawingGraph.Size = new System.Drawing.Size(93, 23);
            this.drawingGraph.TabIndex = 2;
            this.drawingGraph.TabStop = false;
            this.drawingGraph.Text = "Построить";
            this.drawingGraph.UseVisualStyleBackColor = false;
            this.drawingGraph.Click += new System.EventHandler(this.drawingGraph_Click);
            // 
            // draw
            // 
            this.draw.Controls.Add(this.pictureBox1);
            this.draw.Location = new System.Drawing.Point(232, 30);
            this.draw.Name = "draw";
            this.draw.Size = new System.Drawing.Size(403, 375);
            this.draw.TabIndex = 2;
            this.draw.TabStop = false;
            this.draw.Text = "Графическая демострация";
            this.draw.Paint += new System.Windows.Forms.PaintEventHandler(this.drawField_Paint);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(5, 18);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(392, 342);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(650, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сохранитьToolStripMenuItem,
            this.закрытьToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            this.сохранитьToolStripMenuItem.Click += new System.EventHandler(this.сохранитьToolStripMenuItem_Click);
            // 
            // закрытьToolStripMenuItem
            // 
            this.закрытьToolStripMenuItem.Name = "закрытьToolStripMenuItem";
            this.закрытьToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
            this.закрытьToolStripMenuItem.Text = "Закрыть";
            this.закрытьToolStripMenuItem.Click += new System.EventHandler(this.закрытьToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Thistle;
            this.ClientSize = new System.Drawing.Size(650, 414);
            this.Controls.Add(this.draw);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Задача коммивояжёра";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Matrix)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.main_vershina)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vershiny)).EndInit();
            this.draw.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView Matrix;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown vershiny;
        private System.Windows.Forms.GroupBox draw;
        private System.Windows.Forms.Button drawingGraph;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Find;
        private System.Windows.Forms.NumericUpDown main_vershina;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem закрытьToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

